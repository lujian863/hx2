<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/
	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("inc/top.php");
	include("conf/config.php");
	include("classes/conn.class.php");
	include("classes/lib.class.php");
	include("classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	$q = new AllQuery();	
?>
<form name="f1" id="f1" method="get" action="notice_list.php">
	<input type="hidden" name="page_now" value="
	<?php 
		$page_now = 1;
		if(isset($_POST['page_now'])){
			$page_now = $_POST['page_now'];
		}
		if(isset($_GET['page_now'])){
			$page_now = $_GET['page_now'];
		}
		echo $page_now;
	?>" id="page_now" /><!-- 当前是第几页 -->
</form>

<div id="insti_c">
		<div id="gonggao_m">
    	<div id="r_title_wrap1"><div id="r_title1">最新通知</div></div>
        <div id="gonggao_c1">
 			<marquee height="200" direction="up" behavior="scroll" scrollamount="2" scrolldelay="100" onMouseOver="this.stop()" onMouseOut="this.start()" width="214">
 			<ul>
<?php
	$rs_notice = $q->getNoticeList(12);
	while($row_notice = mysql_fetch_array($rs_notice)){
?>
        		<li><a href="notice_view.php?id=<?php echo $row_notice['n_id']; ?>" title="<?php echo $row_notice['n_title']; ?>">
				<?php 
					$tit = $row_notice['n_title'];
					if(mb_strlen($tit,'UTF-8')>16){
						$tit = mb_substr($tit,0,16,'UTF-8');
						$tit .= "...";
					}
					echo $tit; 
				?>
				</a></li>
<?php
	}
?>
        	</ul>
			</marquee>
			</div>
        </div>

		<div id="m_list">
        	<div id="m_title">
			通知公告
			</div>
            <div id="m_cc">
            <ul>
<?php
	$page_num = 16;
	
	//这里获取分页所需的数据
	$total_num = $q->getNoticeNum();//给定新闻类别，获取新闻数量
	$total_page = ceil($total_num/$page_num);//获取总页数

	$rs_notice = $q->getPageNotice($page_now,$page_num);
	while($row_notice = mysql_fetch_array($rs_notice)){
?>           	
				
				<li>
                	<span><a href="notice_view.php?id=<?php echo $row_notice['n_id']; ?>" target="_blank">
					<?php 
						$tit = $row_notice['n_title'];
						if(mb_strlen($tit,'UTF-8')>40){
							$tit = mb_substr($tit,0,40,'UTF-8');
							$tit .= "...";
						}
						echo $tit; 
					?>
					</a></span>
                  <span1>[<?php echo $row_notice['n_time']; ?>]</span1>
                </li>
<?php
	}
?>              
                
            </ul>
            </div>
            <div id="m_next">
        	<ul>						
<?php
	if($page_now != 1){	
?>
		<li><a href="#" onclick="startPage();">首&nbsp;&nbsp;页</a></li>
		<li><a href="#">&nbsp;</a></li>
		<li><a href="#" onclick="lastPage();">上&nbsp;&nbsp;页</a></li>
<?php 
	}
	
	$nums = 10;//设置快速跳转按钮为5个(5页为一行，最后不足5页的为一行)
	
	$page_list_num = ceil($total_page/$nums);//总共有多少行
	$tmp = ceil($page_now/$nums);//当前是在第几行
	$tmp_mo = $nums - $total_page%$nums;//不足一行时后面去掉的
	
	$start_page_num = ($tmp-1)*$nums+1;//起始页码
	$end_page_num = $tmp*$nums;//结束页码
	if($tmp == $page_list_num && $tmp_mo != $nums){//当为最后一行时，减去不足5页的，如果$tmp_mo == 5，则不计
		$end_page_num = $end_page_num - $tmp_mo;
	}
	//for($i = 0; $i < 5; $i++){
	$page_tmp = $start_page_num;
	while($page_tmp <= $end_page_num){
		if($page_tmp != $page_now){
?>
		<li><a href="#" onclick="jumpPage(<?php echo $page_tmp; ?>);"><?php echo $page_tmp; ?></a></li>
<?php
		}else{
			echo "<li><a href=\"#\"><font color=\"#FF0000\" style=\"font-weight:700; font-style:italic;\">".$page_tmp."</font></a></li>";
		}
		$page_tmp++;
	}
	if($page_now < $total_page){	
?>
		<li><a href="#" onclick="nextPage();">下&nbsp;&nbsp;页</a></li>
		<li><a href="#">&nbsp;</a></li>
		<li><a href="#" onclick="endPage(<?php echo $total_page; ?>);">末&nbsp;&nbsp;页</a></li>
<?php 
	}
?>	
        	</ul>
        	</div>
        </div>
</div>

<?php
	include("inc/bottom.php");
?>
<script language="javascript">
function nextPage(){
	var pn = parseInt(document.getElementById("page_now").value) + 1;
	document.getElementById("page_now").value = pn;
	//document.getElementById("f1").action.value = 'notice_list.php?kind=1&page_now='+pn;
	document.getElementById("f1").submit();
}
function jumpPage(pn){
	document.getElementById("page_now").value = pn;
	document.getElementById("f1").submit();
}
function lastPage(){
	var pn = parseInt(document.getElementById("page_now").value) - 1;
	document.getElementById("page_now").value = pn;
	document.getElementById("f1").submit();
}
function startPage(){
	document.getElementById("page_now").value = 1;
	document.getElementById("f1").submit();
}
function endPage(page_end){
	document.getElementById("page_now").value = page_end;
	document.getElementById("f1").submit();
}
</script>
<!-- TianJi Button BEGIN -->
<script type="text/javascript"> 
	var tianji_config = {
		url: "http://redcross.njau.edu.cn/",
		title: "南京农业大学红十字会",
		siteName:"南京农业大学红十字会"
	}
</script>
<script type="text/javascript" src="http://tj.see-say.com/code/tianji_r.js?move=0" charset="utf-8"></script>
<!-- TianJi Button END -->