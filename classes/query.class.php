<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


//所有的操作数据库函数都在这里
class AllQuery{
	
	
	//********* 新闻 **************************************************************************************************
	
	//查询一条新闻
	public function getOneNews($id){
		$sql = "select * from tb_news where n_id = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row;
	}
	
	//查询一页新闻（参数说明：$pnum-第几页，$num-几条，$kind-新闻类别）
	public function getPageNews($pnum,$num,$kind){
		$pa = ($pnum-1) * $num;
		$sql = "select * from tb_news where n_kind = ".$kind." order by n_time desc limit ".$pa.",".$num."";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询前N条新闻，返回记录集
	public function getNewsList($nt,$kind){
		$sql = "select * from tb_news where n_kind = ".$kind." order by n_id desc limit 0,".$nt."";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询前N条图片新闻，如果不足N条，则返回所有
	public function getNewsPicList($nt){
		mysql_query("set names utf8");
		/*
		$sql = "select count(*) from tb_news where n_kind = 1 and n_pic != 'nothing'";
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		if($row[0] < $nt){
			$nt = $row[0];
		}
		*/
		$sql = "select * from tb_news where n_kind = 1 and n_pic != 'nothing' order by n_id desc limit 0,".$nt."";
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//添加一条新闻
	public function addNews($n_title,$n_author,$n_time,$n_content,$n_times,$n_kind,$n_pic){
		$sql = "insert into tb_news (n_title,n_author,n_time,n_content,n_times,n_kind,n_pic) values ";
		if($n_time == ""){
			$sql .=  "('".$n_title."','".$n_author."',now(),'".$n_content."',".$n_times.",'".$n_kind."','".$n_pic."')";
		}else{
			$sql .= "('".$n_title."','".$n_author."','".$n_time."','".$n_content."',".$n_times.",'".$n_kind."','".$n_pic."')";
		}
		//echo $sql;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//更新一条新闻
	public function updateNews($id,$n_title,$n_author,$n_time,$n_content,$n_times,$n_kind){
		$sql = "update tb_news set n_title = '".$n_title."' , n_author =  '".$n_author."' , ";
		$sql .= "n_time = '".$n_time."' , n_times = ".$n_times." , n_content = '".$n_content."' , n_kind = ".$n_kind." ";
		$sql .= "where n_id = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询新闻类别
	public function getKinds(){
		$sql = "select * from tb_kind order by k_num";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}

	//查询单个新闻类别
	public function getOneKind($kind){
		$sql = "select k_name from tb_kind where k_num = ".$kind;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row['k_name'];
	}
	
	//删除一条新闻
	public function delOneNews($id){
		$sql = "delete from tb_news where n_id = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//给定新闻类别，获取新闻数量
	public function getNewsNum($kind){
		$sql = "select count(*) from tb_news where n_kind = ".$kind;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row[0];
	}
	
	//新闻阅读次数增加
	public function addNewsReadTimes($id){
		$sql = "update tb_news set n_times = n_times + 1 where n_id = ".$id."";
		mysql_query("set names utf8");
		mysql_query($sql);
	}
	
	
	//********* 通知 **************************************************************************************************
	
	public function addNoticeReadTimes($id){
		$sql = "update tb_notice set n_times = n_times + 1 where n_id = ".$id."";
		mysql_query("set names utf8");
		mysql_query($sql);
	}
	
	//添加一条通知
	public function addNotice($n_title,$n_author,$n_time,$n_content,$n_times){
		$sql = "insert into tb_notice (n_title,n_author,n_time,n_content,n_times) values ";
		if($n_time == ""){
			$sql .=  "('".$n_title."','".$n_author."',now(),'".$n_content."',".$n_times.")";
		}else{
			$sql .= "('".$n_title."','".$n_author."','".$n_time."','".$n_content."',".$n_times.")";
		}
		//echo $sql;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询前N条通知，返回记录集
	public function getNoticeList($nt){
		$sql = "select * from tb_notice order by n_id desc limit 0,".$nt."";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询一条通知
	public function getOneNotice($id){
		$sql = "select * from tb_notice where n_id = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row;
	}
	
	//查询一页通知（参数说明：$pnum-第几页，$num-几条）
	public function getPageNotice($pnum,$num){
		$pa = ($pnum-1) * $num;
		$sql = "select * from tb_notice order by n_time desc limit ".$pa.",".$num."";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//更新一条通知
	public function updateNotice($id,$n_title,$n_author,$n_time,$n_content,$n_times){
		$sql = "update tb_notice set n_title = '".$n_title."' , n_author =  '".$n_author."' , ";
		$sql .= "n_time = '".$n_time."' , n_times = ".$n_times." , n_content = '".$n_content."' ";
		$sql .= "where n_id = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询通知数量
	public function getNoticeNum(){
		$sql = "select count(*) from tb_notice";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row[0];
	}
	
	//删除一条通知
	public function delOneNotice($id){
		$sql = "delete from tb_notice where n_id = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	
	
/*	
	//查询文件分类
	public function getFileKinds(){
		$sql = "select * from tb_file_kind order by fk_num";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询一条文件分类
	public function getFileKind($id){
		$sql = "select fk_name from tb_file_kind where fk_num = ".$id;
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row[0];
	}
	
	//更新一条文件分类
*/
}
?>