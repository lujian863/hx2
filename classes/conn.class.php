<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


class DBConn{
	
	//构造函数
	function __construct(){
		$this->host = DB_HOST;
		$this->port = DB_PORT;
		$this->database = DB_NAME;
		$this->user = DB_USER;
		$this->password = DB_PASS;
	}
	
	//链接数据库服务器
	function connect_server() {
		@mysql_pconnect($this->host.":".$this->port,$this->user,$this->password)
		or die("无法连接到数据库！请联系开发人员。");
	}

	//链接数据库
	function connect_db() {
		@mysql_select_db($this->database)
		or die("无法连接到指定的数据库！请联系开发人员。");
	}
	
	//关闭连接，实际上不用
	function close() {
		mysql_close();
	}
}
?>