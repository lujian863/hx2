<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


//常用函数库
class Libs{
	
	//网站浏览量增加
	public function addReadTimes(){
		$sql = "update tb_sys set s_see_times = s_see_times + 1 where s_id = 1";
		mysql_query("set names utf8");
		mysql_query($sql);
	}
	
	//获取网站访问量
	public function getReadTimes(){
		$sql = "select s_see_times from tb_sys where s_id = 1";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row[0];
	}
	
	//新闻浏览量增加
	public function newsAddTimes($id){
		$sql = "update tb_news set n_times = n_times + 1 where n_id = ".$id;
		mysql_query("set names utf8");
		mysql_query($sql);
	}
	
	//查询友情链接
	public function getLinks(){
		$sql = "select * from tb_links order by l_num";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询一条友情链接
	public function getOneLink($id){
		$sql = "select * from tb_links where l_num = '".$id."'";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row;
	}
	
	//更新友情链接
	public function updateLinks($id,$name,$url){
		$sql = "update tb_links set l_name = '".$name."', l_url = '".$url."' where l_num = '".$id."' ";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	
	//上传文件<br />
	public function addFile($f_name,$f_path,$f_size){
		$sql = "insert into tb_files (f_times,f_name,f_path,f_time,f_kind,f_size) values (0,'".$f_name."','".$f_path."',now(),'0','".$f_size."')";
		return mysql_query($sql);
	}
	
	//文件大小单位转换
	public function setupSize($fileSize) { 
		$size = sprintf("%u", $fileSize); 
		if($size == 0) { 
			return("0 Bytes"); 
		} 	
		$sizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB"); 
		return round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizename[$i]; 
	}
	
	//查询一页文件
	public function getFiles($pnum,$num){
		$pa = ($pnum-1) * $num;
		$sql = "select * from tb_files order by f_time desc limit ".$pa.",".$num."";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	public function getPageFiles($pnum,$num){
		$pa = ($pnum-1) * $num;
		$sql = "select * from tb_files order by f_time desc limit ".$pa.",".$num."";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//查询文件总数
	public function getFilesNum(){
		$sql = "select count(*) from tb_files";
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row[0];
	}
	
	//********* 关于我们 ***********************************************************************************************
	
	//获取 关于我们 内容
	public function getAboutUs(){
		$sql = "select s_about_us from tb_sys where s_id = 1";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		$row = mysql_fetch_array($rs);
		return $row[0];
	}
	
	//更新 关于我们 内容
	public function updateAboutUs($content){
		$sql = "update tb_sys set s_about_us = '".$content."' where s_id = 1";
		mysql_query("set names utf8");
		return mysql_query($sql);
	}
	
	//********* 管理员列表 ***********************************************************************************************
	public function getAdminList(){
		$sql = "select * from tb_admin order by a_id";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	public function addAdmin($a_name, $a_pass){
		$sql = "insert into tb_admin (a_name, a_password) values ('".$a_name."', '".$a_pass."')";
		mysql_query("set names utf8");
		$rs = mysql_query($sql);
		return $rs;
	}
	
	//更新密码
	public function updateAdmin($a_pass,$id){
		$sql = "update tb_admin set a_password = '".$a_pass."' where a_id = ".$id;
		mysql_query("set names utf8");
		return mysql_query($sql);
	}
	
	//删除
	public function delAdmin($id){
		if($id == 1){
			return false;
		}else{
			$sql = "delete from tb_admin where a_id = ".$id;
			mysql_query("set names utf8");
			$rs = mysql_query($sql);
			return $rs;
		}
	}
	
}
?>