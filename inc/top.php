<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
ProName:redcorss.njau.edu.cn
Time:2012-08-10
Author:lujian863
E-Mail:lujian863@gmail.com
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>南京农业大学红十字会</title>
<link rel="Shortcut Icon" href="images/ico/hx16.ico">
<link href="css/base.css" rel="stylesheet" type="text/css"/>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/index_add.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/time.js"></script>
<script type=text/javascript src="js/jquery.min.js"></script>
<script type=text/javascript src="js/jquery-ui.min.js"></script>
<script language="javascript">
function sor(){
	alert('正在建设中...');
}
</script>
</head>

<body>
<div id="wrap">
<div id="top">
	<div id="top_lef"></div>
	<div id="top_mid"><!--时间-->
		<div id="top_mid_up"></div>
		<div id="top_mid_mid">
			<script language="javascript">
				getMyDate();
			</script>
		</div>
		<div id="top_mid_down"></div>
	</div>
	<div id="top_rig"><!--收藏按钮等-->
		<div id="top_rig_ge">&nbsp;</div>
		<div id="top_rig_lef" align="center"><img src="images/topicons/home.png" /><br />设为首页</div>
		<div id="top_rig_mid" align="center"><img src="images/topicons/star.png" /><br />加入收藏</div>
		<div id="top_rig_rig" align="center"><img src="images/topicons/email.png" /><br />联系我们</div>
	</div>
</div>
<div id="gt"></div>
<div id="top2">
	<div class="content_right">
  		<div class="ad" >
   		 	<ul class="slider" >
      			<li><a href="#"><img src="images/buttom_0.jpg" border="0"/></a></li>
      			<li><a href="#"><img src="images/buttom_1.jpg" border="0"/></a></li>
      			<li><a href="#"><img src="images/buttom_2.jpg" border="0"/></a></li>
      			<li><a href="#"><img src="images/buttom_3.jpg" border="0"/></a></li>
    		</ul>
    		<ul class="num" >
      			<li>1</li>
      			<li>2</li>
      			<li>3</li>
      			<li>4</li>
    		</ul>
  		</div>
	</div>
</div>
<div id="navMenu">
<ul>
    <li class="onelink"><a href="index.php" target="_self">首 页</a></li>
    <li><a href='news_list.php?kind=1'	rel='dropmenu0' target="_self">新闻中心</a></li>
    <li><a href='notice_list.php' 		rel='dropmenu1' target="_self">通知公告</a></li>
    <li><a href='hx_info.php' 			rel='dropmenu2' target="_self">红会简介</a></li>
    <li><a href='news_list.php?kind=3'	rel='dropmenu3' target="_self">志愿者服务</a></li>
    <li><a href='news_list.php?kind=2' 	rel='dropmenu4' target="_self">特色活动</a></li>
    <li><a href='down_list.php' 		rel='dropmenu5' target="_self">下载中心</a></li>
	<li><a href='#' 					rel='dropmenu6' target="_self" onclick="sor();">会员中心</a></li>
</ul>
<div id="navMenu_r"></div>
</div>

<!--
<ul id="dropmenu5" class="dropMenu">
	<li><a href="#" target="_self">资料类别1</a></li>
    <li><a href="#" target="_self">资料类别2</a></li>
    <li><a href="#" target="_self">资料类别3</a></li>
</ul>
<ul id="dropmenu6" class="dropMenu">
	<li><a href="#" target="_blank">登    录</a></li>
    <li><a href="#" target="_blank">内部通知</a></li>
	<li><a href="#" target="_blank">内部下载</a></li>
    <li><a href="#" target="_blank">个人信息</a></li>
</ul>
<script type="text/javascript">cssdropdown.startchrome("navMenu")</script> 
-->
