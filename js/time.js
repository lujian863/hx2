// JavaScript Document
/*
*ProName:redcorss.njau.edu.cn
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/
function getMyDate(){
		var tstr = "";
		var thedate = new Date();
        //记住：一定要使用getFullYear()方法获取年份，避免浏览器的兼容性
        tstr += thedate.getFullYear();
		tstr += '年';
		tstr += (thedate.getMonth()+1);
		tstr += '月';
		tstr += thedate.getDate();
		tstr += '日&nbsp;';
		
        var num = thedate.getDay();
		var str;
		switch(num){
			case 0:
				str = "日";
				break;
			case 1:
				str = "一";
				break;
			case 2:
				str = "二";
				break;
			case 3:
				str = "三";
				break;
			case 4:
				str = "四";
				break;
			case 5:
				str = "五";
				break;
			case 6:
				str = "六";
				break;
		}
		tstr +='星期';
		tstr += str;
		document.write(tstr);
}