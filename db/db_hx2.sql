-- MySQL dump 10.13  Distrib 5.5.28, for Win32 (x86)
--
-- Host: localhost    Database: db_hx2
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_admin`
--

DROP TABLE IF EXISTS `tb_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_admin` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `a_name` varchar(45) DEFAULT NULL,
  `a_password` varchar(100) DEFAULT NULL,
  `a_last_time` datetime DEFAULT NULL,
  `a_last_ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_admin`
--

LOCK TABLES `tb_admin` WRITE;
/*!40000 ALTER TABLE `tb_admin` DISABLE KEYS */;
INSERT INTO `tb_admin` VALUES (1,'admin','7fef6171469e80d32c0559f88b377245',NULL,NULL),(2,'admin2','admin',NULL,NULL);
/*!40000 ALTER TABLE `tb_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_file_kind`
--

DROP TABLE IF EXISTS `tb_file_kind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_file_kind` (
  `fk_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_name` varchar(50) DEFAULT NULL,
  `fk_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`fk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_file_kind`
--

LOCK TABLES `tb_file_kind` WRITE;
/*!40000 ALTER TABLE `tb_file_kind` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_file_kind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_files`
--

DROP TABLE IF EXISTS `tb_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_files` (
  `f_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(200) DEFAULT NULL,
  `f_size` varchar(45) DEFAULT NULL,
  `f_path` varchar(200) DEFAULT NULL,
  `f_kind` int(11) DEFAULT NULL,
  `f_time` datetime DEFAULT NULL,
  `f_times` int(11) DEFAULT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_files`
--

LOCK TABLES `tb_files` WRITE;
/*!40000 ALTER TABLE `tb_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_info`
--

DROP TABLE IF EXISTS `tb_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_info` (
  `i_id` int(11) NOT NULL,
  `i_title` varchar(200) DEFAULT NULL,
  `i_author` varchar(45) DEFAULT NULL,
  `i_time` datetime DEFAULT NULL,
  `i_content` varchar(15000) DEFAULT NULL,
  `i_times` int(11) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_info`
--

LOCK TABLES `tb_info` WRITE;
/*!40000 ALTER TABLE `tb_info` DISABLE KEYS */;
INSERT INTO `tb_info` VALUES (1,'测试','测试','2012-07-24 00:00:00','测试测试',0);
/*!40000 ALTER TABLE `tb_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kind`
--

DROP TABLE IF EXISTS `tb_kind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_kind` (
  `k_id` int(11) NOT NULL AUTO_INCREMENT,
  `k_name` varchar(100) DEFAULT NULL,
  `k_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`k_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kind`
--

LOCK TABLES `tb_kind` WRITE;
/*!40000 ALTER TABLE `tb_kind` DISABLE KEYS */;
INSERT INTO `tb_kind` VALUES (1,'新闻中心',1),(2,'特色活动',2),(3,'志愿者服务',3);
/*!40000 ALTER TABLE `tb_kind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_links`
--

DROP TABLE IF EXISTS `tb_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_links` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `l_name` varchar(40) DEFAULT NULL,
  `l_url` varchar(200) DEFAULT NULL,
  `l_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_links`
--

LOCK TABLES `tb_links` WRITE;
/*!40000 ALTER TABLE `tb_links` DISABLE KEYS */;
INSERT INTO `tb_links` VALUES (1,'绿岛网络开发协会·','http://lvdao.njau.edu.cn/',1),(2,'南农11111','http://www.njau.edu.cn',2),(3,'南京农业大学','http://www.njau.edu.cn',3),(4,'红十字国际委员会','http://www.icrc.org/chi/',4),(5,'南京市红十字会','http://www.njredcross.org.cn/',5),(6,'yyy','yyyy',6),(7,'南京市红十字会','http://www.njredcross.org.cn/',7),(8,'南京市红十字会','http://www.njredcross.org.cn/',8);
/*!40000 ALTER TABLE `tb_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_news`
--

DROP TABLE IF EXISTS `tb_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_news` (
  `n_id` int(11) NOT NULL AUTO_INCREMENT,
  `n_title` varchar(300) DEFAULT NULL,
  `n_author` varchar(50) DEFAULT NULL,
  `n_time` datetime DEFAULT NULL,
  `n_content` varchar(20000) DEFAULT NULL,
  `n_times` int(11) DEFAULT NULL,
  `n_kind` int(11) DEFAULT NULL,
  `n_pic` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_news`
--

LOCK TABLES `tb_news` WRITE;
/*!40000 ALTER TABLE `tb_news` DISABLE KEYS */;
INSERT INTO `tb_news` VALUES (129,'新闻测试','南农红会','2012-09-12 11:28:42','<p>\r\n	新闻测试\r\n</p>\r\n<p>\r\n	<img src=\"/hx2/admin/kindeditor/attached/image/20120912/20120912112908_48674.jpg\" alt=\"\" />\r\n</p>',1,1,'20120912112908_48674.jpg'),(130,'特色活动测试','南农红会','2012-09-12 11:29:12','<p>\r\n	特色活动测试\r\n</p>\r\n<p>\r\n	<img src=\"/hx2/admin/kindeditor/attached/image/20120912/20120912112949_84284.jpg\" alt=\"\" />\r\n</p>',0,2,'20120912112949_84284.jpg'),(131,'志愿者服务测试','南农红会','2012-09-12 11:29:58','志愿者服务测试<br />\r\n<p>\r\n	<img src=\"/hx2/admin/kindeditor/attached/image/20120912/20120912113036_44343.jpg\" alt=\"\" />\r\n</p>',2,3,'20120912113036_44343.jpg');
/*!40000 ALTER TABLE `tb_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_notice`
--

DROP TABLE IF EXISTS `tb_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_notice` (
  `n_id` int(11) NOT NULL AUTO_INCREMENT,
  `n_title` varchar(200) DEFAULT NULL,
  `n_author` varchar(50) DEFAULT NULL,
  `n_time` datetime DEFAULT NULL,
  `n_content` varchar(20000) DEFAULT NULL,
  `n_times` int(11) DEFAULT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_notice`
--

LOCK TABLES `tb_notice` WRITE;
/*!40000 ALTER TABLE `tb_notice` DISABLE KEYS */;
INSERT INTO `tb_notice` VALUES (3,'通知测试','南农红会','2012-08-21 16:07:15','通知测试',0);
/*!40000 ALTER TABLE `tb_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pic`
--

DROP TABLE IF EXISTS `tb_pic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pic` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(100) DEFAULT NULL,
  `p_path` varchar(100) DEFAULT NULL,
  `p_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pic`
--

LOCK TABLES `tb_pic` WRITE;
/*!40000 ALTER TABLE `tb_pic` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_pic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_sys`
--

DROP TABLE IF EXISTS `tb_sys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sys` (
  `s_id` int(11) NOT NULL,
  `s_see_times` int(11) DEFAULT NULL,
  `s_login_times` int(11) DEFAULT NULL,
  `s_about_us` varchar(20000) DEFAULT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_sys`
--

LOCK TABLES `tb_sys` WRITE;
/*!40000 ALTER TABLE `tb_sys` DISABLE KEYS */;
INSERT INTO `tb_sys` VALUES (1,455,NULL,'<div>\r\n	<span style=\"font-size:medium;\"><span style=\"color:black;\">&nbsp;&nbsp;&nbsp;&nbsp;南京农业大学红十字会成立于1982年，隶属于南京农业大学团委，南京市玄武区红十字会，以弘扬人道主义，普及救护技能，组织保健教育，促进校园文明为工作中心，坚持人道、公正、中立、独立、志愿服务、统一、普遍七项基本原则，以完成人道，博爱，奉献三大宗旨。南京农业大学红十字会坚持走依法建会、依法治会、依法兴会的</span></span><span style=\"font-size:medium;\"><span style=\"color:black;\">道</span><span style=\"color:black;\">路，加强法制建设和组织、机关建设，红十字会各级组织得到了巩固和发展。</span></span> \r\n</div>\r\n<div>\r\n	<span style=\"font-size:medium;\">&nbsp;&nbsp;&nbsp;&nbsp;红会于1982年成立以后，中间因各种原因暂时中断，直到1998年才恢复工作。恢复后组织迅速返发展，会员人数大量增加，采用了分会制度。</span> \r\n</div>\r\n<div>\r\n	<span style=\"font-size:medium;\">&nbsp;&nbsp;&nbsp;&nbsp;自2003年赞助2名小学生，2004年发展志愿者宿舍至今，其已成为我校特色项目，也是资助金的主要来源之一；</span> \r\n</div>\r\n<div>\r\n	<span style=\"font-size:medium;\"><span style=\"color:black;\">为全面贯彻落实“红十字会法”，保障红十字会依法履行职责，促进红十字事业的深入健康发展，</span><span style=\"color:black;\">南京农业大学红十字会设有《南京农业大学红十字会章程》《南京农业大学红十字会优秀会员考核制度》《南京农业大学优秀干部考核制度》等规章制度作为基准，严格要求红十字会的领导帆布，使我会活动有序、规范地开展，工作以最大效益进行;设有精密的财务管理制度，使我会经费开支透明，</span><span style=\"color:black;\">有法可依，有章可循</span><span style=\"color:black;\">。</span></span> \r\n</div>\r\n<div>\r\n	<span style=\"font-size:medium;\">&nbsp;&nbsp;&nbsp;&nbsp;根据《关于加强南京农业大学红十字会建设的意见》（校发[2006]360号）文件精神，<span style=\"color:black;\">学校红十字会的最高权力机构是会员代表大会，会员代表大会闭会期间，由理事会执行相关决议，并组建了以校党委副书记为会长、理事长，各相关职能部门负责同志和各学院党委副书记为理事的领导机构，校红十字会秘书长由校团委书记担任，负责管理和指导红十字会工作。校红十字学生分会</span>下设秘书处、宣传部、外联部、组织部、记者团、急救队和志愿者服务队7个正式部门，各部门互相监督协作,将工作发挥最大效应。</span> \r\n</div>\r\n<div>\r\n	<span style=\"font-size:medium;\"><span style=\"color:black;\">&nbsp;&nbsp; &nbsp;</span><span style=\"color:black;\">自2007年部署建立各分院红十字会的工作后，2008年校红十字会对各院分红十字会进行进一步的法制管理，设立了《南京农业大学优秀红十字分会考核制度》，规范各分会工作，并指导其开展一系列卓有成效得活动</span><span style=\"color:black;\">，达到院院有特色，院院出成果</span><span style=\"color:black;\">。</span><span style=\"color:black;\">截止到9月底，在各学院分会积极努力的工作下，我校红十字会新老会员</span><span style=\"color:black;\">的总人数高达1770人次，新会员人数835人，达到新生入学率的20%</span><span style=\"color:black;\">，会费收缴率高达100%。</span></span> \r\n</div>\r\n<div>\r\n	<span style=\"font-size:medium;\"><span style=\"color:black;\">&nbsp;&nbsp; &nbsp;</span><span style=\"color:black;\">南京农业大学</span><span style=\"color:black;\">红十字会工作在</span><span style=\"color:black;\">市红会</span><span style=\"color:black;\">、</span><span style=\"color:black;\">区红会、校团委</span><span style=\"color:black;\">的正确领导下，在</span><span style=\"color:black;\">上级</span><span style=\"color:black;\">红十字会的具体指导下，</span><span style=\"color:black;\">在校医院的帮助下，在老师和全体会员们的共同努力下，</span><span style=\"color:black;\">坚持“抓重点、干实事、见效果”的原则，</span><span style=\"color:black;\">与时俱进，</span><span style=\"color:black;\">以“</span><span style=\"color:black;\">奉</span><span style=\"color:black;\">献爱心、</span><span style=\"color:black;\">抗震救灾</span><span style=\"color:black;\">”</span><span style=\"color:black;\">、“迎绿色奥运，做文明先锋”</span><span style=\"color:black;\">为主题，大力开展宣传动员工作，广泛募集救助资金，持续开展救灾、帮困和助学活动，</span><span style=\"color:black;\">创建各种有红十字会特色得志愿服务活动，积极宣传红十字会法，</span><span style=\"color:black;\">充分发挥</span><span style=\"color:black;\">我校红十字会</span><span style=\"color:black;\">人道事务工作方面的作用，各项工作取得了突破性进展</span><span style=\"color:black;\">，荣获南京农业大学十佳社团、南京农业大学十佳活动、南京市高校急救知识竞赛三等奖、南京市抗震救灾</span><span style=\"color:black;\">先进</span><span style=\"color:black;\">集体、南京农业大学优秀志愿服务组织奖等荣誉称号，并获得南京市血液中心授予的锦旗</span><span style=\"color:black;\">。</span></span> \r\n</div>');
/*!40000 ALTER TABLE `tb_sys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(45) DEFAULT NULL,
  `u_tname` varchar(45) DEFAULT NULL,
  `u_pass` varchar(45) DEFAULT NULL,
  `u_part` varchar(100) DEFAULT NULL,
  `u_info` varchar(300) DEFAULT NULL,
  `u_img` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,'aaa','张三','aaa','1','暂无',NULL);
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-10-18  0:44:26
