<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("inc/top.php");
	include("conf/config.php");
	include("classes/conn.class.php");
	include("classes/lib.class.php");
	include("classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	$q = new AllQuery();
	
	extract($_POST);
	extract($_GET);
	unset($_POST,$_GET);
	
	$row = $q->getOneNotice($id);
	$q->addNoticeReadTimes($id);
?>

<div id="insti_c">
		<div id="gonggao_m">
    	<div id="r_title_wrap1"><div id="r_title1">最新通知</div></div>
        <div id="gonggao_c1">
 			<marquee height="200" direction="up" behavior="scroll" scrollamount="2" scrolldelay="100" onMouseOver="this.stop()" onMouseOut="this.start()" width="214">
 			<ul>
<?php
	$rs_notice = $q->getNoticeList(12);
	while($row_notice = mysql_fetch_array($rs_notice)){
?>
        		<li><a href="notice_view.php?id=<?php echo $row_notice['n_id']; ?>" title="<?php echo $row_notice['n_title']; ?>">
				<?php 
					$tit = $row_notice['n_title'];
					if(mb_strlen($tit,'UTF-8')>16){
						$tit = mb_substr($tit,0,16,'UTF-8');
						$tit .= "...";
					}
					echo $tit; 
				?>
				</a></li>
<?php
	}
?>
        	</ul>
			</marquee>
			</div>
        </div>

		<div id="insti_list">
        	<div id="insti_title">
				<div id="news_title">
					<?php echo $row['n_title']; ?>
				</div>
				<div id="news_info">
					通知来源：<?php echo $row['n_author']; ?>&nbsp;&nbsp;&nbsp;
					发表时间：<?php echo $row['n_time']; ?>&nbsp;&nbsp;&nbsp;
					阅览次数：<?php echo $row['n_times']; ?>次
				</div>
			</div>
			<div id="insti_cc">
				<?php echo $row['n_content']; ?>
			</div>
        </div>
</div>


<?php
	include("inc/bottom.php");
?>
<!-- TianJi Button BEGIN -->
<script type="text/javascript"> 
	var tianji_config = {
		url: "http://redcross.njau.edu.cn/",
		title: "南京农业大学红十字会",
		siteName:"南京农业大学红十字会"
	}
</script>
<script type="text/javascript" src="http://tj.see-say.com/code/tianji_r.js?move=0" charset="utf-8"></script>
<!-- TianJi Button END -->