<?php
	include("cfm.php");
	
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$q = new AllQuery();
	if($q->delOneNews($_GET['id'])){
		echo "<script>parent.document.getElementById('mainframe').contentWindow.location = 'news_list.php'</script>";
		exit();
	}else{
		echo "<script>alert('新闻删除失败，请联系管理员！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
?>