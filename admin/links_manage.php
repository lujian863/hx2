<?php
	include("cfm.php");
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>友情链接</title>
<link rel="stylesheet" href="css/wel.css" />
</head>

<body>
<h2>友情链接</h2>
<hr />
<div class="link_list">
<?php
 	$rs = $lib->getLinks();
	while($row = mysql_fetch_array($rs)){
?>
	<div class="link_li">
		<div class="link_pos"><?php echo $row['l_num']; ?>号位</div>
		<div class="link_name"><?php echo $row['l_name']; ?></div>
		<div class="link_url">
		<?php 
			$tit = $row['l_url'];
			if(mb_strlen($tit,'UTF-8')>20){
				$tit = mb_substr($tit,0,20,'UTF-8');
				$tit .= "...";
			}
			echo $tit; 
		?>
		</div>
		<div class="link_btn"><input type="button" value="修改本条链接" class="btn" onclick="editLink('<?php echo $row['l_num']; ?>')" /></div>
	</div>
<?php
	}
?>
</div>
<script language="javascript">
function editLink(id){
	window.open('links_edit.php?id='+id,'_blank','scrollbars=yes,width=580,height=220');
}
</script>
</body>
</html>
