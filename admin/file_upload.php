<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>文件上传</title>
<link rel="stylesheet" href="css/wel.css" />

<!--datePicker-->
<script type="text/javascript" src="datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="datePicker/config.js"></script>

</head>

<body>
<h2>文件上传</h2>
<hr />
<?php
$uptypes=array('image/jpg',  //上传文件类型列表
 'image/jpeg',
 'image/png',
 'image/pjpeg',
 'image/gif',
 'image/bmp',
 'image/x-png',
 'application/x-shockwave-flash',
 'application/msword',
 'application/msexcel',
 'audio/x-ms-wma',
 'audio/mp3',
 'application/vnd.rn-realmedia',
 'application/x-zip-compressed',
 'application/octet-stream', //rar文件
 'application/zip',
 'text/plain',
 'application/vnd.ms-powerpoint',
 'application/pdf'
 );

$max_file_size=100000000;   //上传文件大小限制, 单位BYTE
$path_parts=pathinfo($_SERVER['PHP_SELF']); //取得当前路径
$destination_folder="../upload/"; //上传文件路径
$watermark=0;   //是否附加水印(1为加水印,0为不加水印);
$watertype=1;   //水印类型(1为文字,2为图片)
$waterposition=2;   //水印位置(1为左下角,2为右下角,3为左上角,4为右上角,5为居中);
$waterstring="sports_njau"; //水印字符串
$waterimg="xplore.gif";  //水印图片
$imgpreview=0;   //是否生成预览图(1为生成,0为不生成);
$imgpreviewsize=1/2;  //缩略图比例
?>
<form name="f1" method="post" enctype="multipart/form-data" onsubmit="return chk();">
<div class="fmain">
	<div class="fang">
		<div class="fang_lef">
			文件名称：
		</div>
		<div class="fang_rig">
			<input type="text" name="f_name" id="f_name" class="finp" />
		</div>
	</div>
<!--
	<div class="fang">
		<div class="fang_lef">
			文件类别：
		</div>
		<div class="fang_rig">
			<select name="f_kind" id="f_kind" class="finp">
				<option value="1">类别1</option>
				<option value="1">类别2</option>
				<option value="1">类别3</option>
			</select>
		</div>
	</div>
-->	
	<div class="fang">
		<div class="fang_lef">
			选择文件：
		</div>
		<div class="fang_rig">
			<!--
			<input type="text" name="f_path" id="f_path" class="finp" />
			<input type="button" value="浏&nbsp;&nbsp;览" onclick="myfile.click();" id="btn" class="btn" />
			<input type="file" id="myfile" onchange="f_path.value=this.value" style="display:none" />	
			-->
			<input type="file" name="f_path" id="f_path" />
				
		</div>
	</div>
	
	<div class="gang">
		<p><b>文件上传说明：</b></p>
		<p>1.允许上传的文件类型有:rar|zip|doc|xls|ppt|pdf|txt|jpg|jpeg|gif|bmp|png|mp3 。</p>
		<p>2.由于原文件在上传时会被重命名，所以这里需要输入文件名称。这里输入的文件名称将会在前台显示。</p>
		<p>3.文件大小不得超过10M，建议上传文件大小低于5M。</p>
	</div>

	<div class="fang">
		<div class="fang_lef"></div>
		<div class="fang_rig">
			<div style="margin-right:10%; float:right;">
			<input type="submit" value="开始上传" id="btn" class="btn"/>
			</div>
		</div>
	</div>
	<div class="gang" id="gang">

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
if (!is_uploaded_file($_FILES["f_path"]["tmp_name"]))
//是否存在文件
{
echo "<font color='red'>文件不存在！</font>";
exit;
}

 $file = $_FILES["f_path"];
 if($max_file_size < $file["size"])
 //检查文件大小
 {
 echo "<font color='red'>文件太大！</font>";
 exit;
  }

if(!in_array($file["type"], $uptypes))
//检查文件类型
{
 echo "<font color='red'>不能上传此类型文件！</font>";
 exit;
}

if(!file_exists($destination_folder))
mkdir($destination_folder);

$filename=$file["tmp_name"];
$image_size = getimagesize($filename);
$pinfo=pathinfo($file["name"]);
$ftype=$pinfo["extension"];
$destination = $destination_folder.time().".".$ftype;
if (file_exists($destination) && $overwrite != true)
{
     echo "<font color='red'>同名文件已经存在了！</a>";
     exit;
  }

 if(!move_uploaded_file ($filename, $destination))
 {
   echo "<font color='red'>移动文件出错！</a>";
     exit;
  }

$pinfo=pathinfo($destination);
$fname=$pinfo["basename"];
echo " <font color=red> <br /><b>文件上传成功!</b> <br /><br />注意：请不要刷新此页面，否则将会重复上传！！！</font><br>";

if($watermark==1)
{
$iinfo=getimagesize($destination,$iinfo);
$nimage=imagecreatetruecolor($image_size[0],$image_size[1]);
$white=imagecolorallocate($nimage,255,255,255);
$black=imagecolorallocate($nimage,0,0,0);
$red=imagecolorallocate($nimage,255,0,0);
imagefill($nimage,0,0,$white);
switch ($iinfo[2])
{
 case 1:
 $simage =imagecreatefromgif($destination);
 break;
 case 2:
 $simage =imagecreatefromjpeg($destination);
 break;
 case 3:
 $simage =imagecreatefrompng($destination);
 break;
 case 6:
 $simage =imagecreatefromwbmp($destination);
 break;
 default:
 die("<font color='red'>不能上传此类型文件！</a>");
 exit;
}

imagecopy($nimage,$simage,0,0,0,0,$image_size[0],$image_size[1]);
imagefilledrectangle($nimage,1,$image_size[1]-15,80,$image_size[1],$white);


switch ($iinfo[2])
{
 case 1:
 //imagegif($nimage, $destination);
 imagejpeg($nimage, $destination);
 break;
 case 2:
 imagejpeg($nimage, $destination);
 break;
 case 3:
 imagepng($nimage, $destination);
 break;
 case 6:
 imagewbmp($nimage, $destination);
 //imagejpeg($nimage, $destination);
 break;
}

//覆盖原上传文件
imagedestroy($nimage);
imagedestroy($simage);
}

	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	$lib->addFile($_POST['f_name'],$fname,$file["size"]);
}
?>
	</div>
</div>
</form>
</body>
</html>
<script language="javascript">
function chk(){
	 if(document.getElementById('f_name').value == ''){
	 	alert('请填写文件名称！');
		return false;
	 }
	 
	 if(document.getElementById('f_path').value == ''){
	 	alert('请选择文件！');
		return false;
	 }
	 
	 return true;
}
</script>