<?php
	$id = $_GET['id'];
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	$row = $lib->getOneLink($id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改友情链接</title>
<link rel="stylesheet" href="css/wel.css" />
</head>

<body>
<h3>修改友情链接</h3>
<hr />
<p align="center">
<?php echo $id; ?>号位
</p>
<form name="f1" method="post" action="link_edit_sub.php">
<input type="hidden" name="l_num" value="<?php echo $id; ?>" />
<table width="488" border="0">
  <tr>
    <td width="113">链接名称：</td>
    <td width="365"><input type="text" name="l_name" value="<?php echo $row['l_name']; ?>"  class="minp" /></td>
  </tr>
  <tr>
    <td>链接地址：</td>
    <td><input type="text" name="l_url" value="<?php echo $row['l_url']; ?>"  class="minp" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><input type="submit" value="提&nbsp;&nbsp;交" class="btn" /></td>
  </tr>
</table>
</form>
</body>
</html>
