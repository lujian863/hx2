<?php
	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	
	$content = $_POST['s_about_us'];
	
	if($lib->updateAboutUs($content)){
		echo "<script>alert('更新成功！');window.location.href='about_us.php';</script>";
		exit();
	}else{
		echo "<script>alert('更新失败！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
?>