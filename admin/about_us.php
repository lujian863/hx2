<?php
	include("cfm.php");
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	$content = $lib->getAboutUs();
	
	$htmlData = $content;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
ProName:redcorss.njau.edu.cn
Time:2012-08-10
Author:lujian863
E-Mail:lujian863@gmail.com
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>关于我们</title>
<link rel="stylesheet" href="css/main.css" />

<!--kindeditor-->
<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="kindeditor/plugins/code/prettify.js"></script>
<script>
	KindEditor.ready(function(K) {
		var editor1 = K.create('textarea[name="s_about_us"]', {
			cssPath : 'kindeditor/plugins/code/prettify.css',
			uploadJson : 'kindeditor/php/upload_json.php',
			fileManagerJson : 'kindeditor/php/file_manager_json.php',
			allowFileManager : true,
			afterCreate : function() {
				var self = this;
				K.ctrl(document, 13, function() {
					self.sync();
					K('form[name=f1]')[0].submit();
				});
				K.ctrl(self.edit.doc, 13, function() {
					self.sync();
					K('form[name=f1]')[0].submit();
				});
			},
	        //下面这行代码就是关键的所在，当失去焦点时执行 this.sync();解决KindEditor用js提交表单无数据问题
			//sync()：将编辑器的内容设置到原来的textarea控件里。
			afterBlur : function(){
					this.sync();
			}
		});
		
		//prettyPrint();
	});
</script>
</head>

<body>
<h2>关于我们</h2>
<hr />

<div class="bcol" id="c1">
	<p><input type="submit" value="更新内容" class="button"  onclick="showHid();" /></p>
	<?php
		echo $htmlData;
	?>
</div>
<form name="f1" method="post" action="about_us_update.php" id="f1" onsubmit="return checkForm();" >
<div class="bcol" id="c2" style="display:none;">
	<textarea name="s_about_us" style="width:100%;height:500px;visibility:hidden;" id="s_about_us">
		<?php echo htmlspecialchars($htmlData); ?>
	</textarea>
</div>

<div class="sub_btn" id="c3" style="display:none;">
	<input type="button" value="取&nbsp;&nbsp;消" class="button" onclick="backPage();" />&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" value="更&nbsp;&nbsp;新" class="button" />
</div>
</form>
</body>
</html>
<script language="javascript">
	function showHid(){
		document.getElementById('c1').style.display = "none";
		document.getElementById('c2').style.display = "block";
		document.getElementById('c3').style.display = "block";
	}
	
	function backPage(){
		window.location='about_us.php';
	}
	
	function checkForm(){
		if(document.getElementById('s_about_us').value == ''){
			alert('内容不能为空！');
			return false;
		}else{
			return true;
		}
	}
</script>