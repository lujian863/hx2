<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统信息</title>
<style type="text/css">
.sys_info{
	font-size:18px;
	font-family:"宋体";
	color:#009900;
}
.sys_info li{
	height:22px;
	margin-top:5px;
}
</style>
</head>

<body>
<h2>系统信息</h2>
<hr />
<div class="sys_info">
<ul>
	<li>系统版本：1.0</li>
	<li>更新日期：2012年9月10日</li>
	<li>技术支持：南京农业大学绿岛网络开发协会</li>
	<li>联系邮箱：lvdaonet@qq.com</li>
	<li>绿岛网址：http://www.lvdaonet.cn/   http://lvdao.njau.edu.cn/</li>
</ul>
</div>
</body>
</html>
