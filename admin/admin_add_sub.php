<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$lib = new Libs();
	
	extract($_POST);
	extract($_GET);
	unset($_POST,$_GET);
	
	//最后一道验证
	if($a_name == "admin"){
		echo "<script>alert('对不起，账户名不能为admin！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
	
	if($a_name == "" || $a_pass_a == "" || $a_pass_b == "" ){
		echo "<script>alert('管理员添加失败，请检查信息完整性！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
	
	if($a_pass_a != $a_pass_b){
		echo "<script>alert('密码两次输入不一致！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
	
	
	if($lib->addAdmin($a_name, $a_pass_a)){
		echo "<script>alert('管理员添加成功！');window.location.href='admin_add.php';</script>";
		exit();
	}else{
		echo "<script>alert('管理员添加失败，请检数据库连接！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
?>