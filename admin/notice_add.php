<?php
	include("cfm.php");

	$htmlData = '';
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$q = new AllQuery();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
ProName:redcorss.njau.edu.cn
Time:2012-08-10
Author:lujian863
E-Mail:lujian863@gmail.com
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加通知</title>
<link rel="stylesheet" href="css/main.css" />

<!--datePicker-->
<script type="text/javascript" src="datePicker/WdatePicker.js"></script>
<script type="text/javascript" src="datePicker/config.js"></script>

<!--kindeditor-->
<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="kindeditor/plugins/code/prettify.js"></script>
<script>
	KindEditor.ready(function(K) {
		var editor1 = K.create('textarea[name="n_content"]', {
			cssPath : 'kindeditor/plugins/code/prettify.css',
			uploadJson : 'kindeditor/php/upload_json.php',
			fileManagerJson : 'kindeditor/php/file_manager_json.php',
			allowFileManager : true,
			afterCreate : function() {
				var self = this;
				K.ctrl(document, 13, function() {
					self.sync();
					K('form[name=fr_notice_add]')[0].submit();
				});
				K.ctrl(self.edit.doc, 13, function() {
					self.sync();
					K('form[name=fr_notice_add]')[0].submit();
				});
			},
	        //下面这行代码就是关键的所在，当失去焦点时执行 this.sync();解决KindEditor用js提交表单无数据问题
			//sync()：将编辑器的内容设置到原来的textarea控件里。
			afterBlur : function(){
					this.sync();
			}
		});
		
		//prettyPrint();
	});
</script>
</head>

<body onload="init();" >
<h2>添加通知</h2>
<hr />
<form name="fr_notice_add" method="post" action="notice_add_sub.php" id="fr_notice_add" onsubmit="return checkForm();" >
<div class="xcol">
	<div class="xcol-lef">通知标题：</div>
	<div class="xcol-rig"><input type="text" name="n_title" class="inp" id="n_title" onblur="check('n_title','n_title_info');" />
		<div id="n_title_info" style="float:left;"></div>
	</div>
</div>
<div class="xcol">
	<div class="xcol-lef">通知来源：</div>
	<div class="xcol-rig"><input type="text" name="n_author" class="inp" value="南农红会" id="n_author" onblur="check('n_author','n_author_info');" />
		<div id="n_author_info" style="float:left;"></div>
	</div>
</div>
<div class="xcol">
	<div class="xcol-lef">发布时间：</div>
	<div class="xcol-rig"><input type="text" name="n_time" class="inp"  id="n_time" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});" /><font color="#FF0000">*提示：默认为系统时间。</font></div>
</div>

<div class="bcol">
	<div class="bcol-lef">通知内容： </div>
	<div class="bcol-rig">
		<textarea name="n_content" style="width:100%;height:500px;visibility:hidden;" id="n_content">
			<?php echo htmlspecialchars($htmlData); ?>
		</textarea>
	</div>
</div>

  
<div class="xcol">
	<div class="xcol-lef">阅览次数：</div>
	<div class="xcol-rig"><input type="text" name="n_times" class="inp" value="0" onblur="checkNull('n_times','0');" id="n_times" /><font color="#FF0000">*提示：一般情况无须修改。</font></div>
</div>

<div class="sub_btn">
	<!--
	<input type="button" value="预览通知" class="button" onclick="preview();" style="margin-right:30px;" />
	-->
	<input type="submit" value="提&nbsp;&nbsp;交" class="button" />
</div>
</form>
</body>
</html>
<script language="javascript">
function init(){
	var date = new Date();        
	document.getElementById("n_time").value = date.pattern("yyyy-MM-dd hh:mm:ss");  	
}

function preview(){
	//预览通知
	//document.getElementById('fr_notice_add').method='get';
	//下面这一句是解决Kindeditor通过JS提交获取不到数据问题
	//document.getElementById('n_content').value=KindEditor.util.getData('n_content');
	//editor1.sync();
	document.getElementById('fr_notice_add').target='_blank';
	document.getElementById('fr_notice_add').action='test.php';
	document.getElementById('fr_notice_add').submit();
	document.getElementById('fr_notice_add').target='_self';
	document.getElementById('fr_notice_add').action='notice_add_sub.php';
}

Date.prototype.pattern=function(fmt) {           
    var o = {           
    "M+" : this.getMonth()+1, //月份           
    "d+" : this.getDate(), //日           
    "h+" : this.getHours(), //小时           
    "H+" : this.getHours(), //小时           
    "m+" : this.getMinutes(), //分           
    "s+" : this.getSeconds(), //秒           
    "q+" : Math.floor((this.getMonth()+3)/3), //季度           
    "S" : this.getMilliseconds() //毫秒           
    };           
    var week = {           
    "0" : "/u65e5",           
    "1" : "/u4e00",           
    "2" : "/u4e8c",           
    "3" : "/u4e09",           
    "4" : "/u56db",           
    "5" : "/u4e94",           
    "6" : "/u516d"          
    };           
    if(/(y+)/.test(fmt)){           
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));           
    }           
    if(/(E+)/.test(fmt)){           
        fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[this.getDay()+""]);           
    }           
    for(var k in o){           
        if(new RegExp("("+ k +")").test(fmt)){           
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));           
        }           
    }           
    return fmt;           
}

//js验证表单
function checkForm(){
	if(document.getElementById('n_title').value.trim().length == 0){
		alert("通知标题不能为空！");
		document.getElementById('n_title').focus();
		return false;
	}
	
	if(document.getElementById('n_author').value.trim().length == 0){
		alert("通知发布者不能为空！");
		document.getElementById('n_author').focus();
		return false;
	}
	
	if(document.getElementById('n_time').value.trim().length == 0){
		alert("通知发布时间不能为空！");
		document.getElementById('n_time').focus();
		return false;
	}
	
	if(document.getElementById('n_content').value.trim().length == 0){
		alert("通知内容不能为空！");
		document.getElementById('n_content').focus();
		return false;
	}
	
}

function check(inp,inf){
	if(document.getElementById(inp).value == ""){
		document.getElementById(inf).innerHTML='<font color=\"#FF0000\">*提示：此项为必填项！</font>';
	}else{
		document.getElementById(inf).innerHTML='';
	}
}
function checkNull(na,va){
	if(document.getElementById(na).value == ""){
		document.getElementById(na).value = va;
	}
}
</script>
