<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$qur = new AllQuery();
	
	extract($_POST);
	extract($_GET);
	unset($_POST,$_GET);
	
	//最后一道验证
	if($n_title == "" || $n_author == "" || $n_time == "" || $n_content == "" || $n_times == ""  || $n_kind == ""){
		echo "<script>alert('新闻添加失败，请检查信息完整性！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
	
	$img_name = "nothing";
		
	//获取新闻第一张图片的路径
	preg_match_all("/<img.*\>/isU",$n_content,$ereg);//把图片的<img整个都获取出来了
	if($ereg[0] != null){
		$img = $ereg[0][0];//图片  
		preg_match_all('|src="(.*)"|isU', $img, $img1); 
		$img_path = $img1[1][0];//获取第一张图片路径 
		$tmp = explode("/", $img_path);
		$max = count($tmp);
		$img_name = $tmp[$max-1];
	}
	
	if($qur->addNews($n_title,$n_author,$n_time,$n_content,$n_times,$n_kind,$img_name)){
		echo "<script>alert('新闻添加成功！');window.location.href='news_add.php';</script>";
		exit();
	}else{
		echo "<script>alert('新闻添加失败，请检数据库连接！');parent.document.getElementById('mainframe').contentWindow.history.back(); </script>";
		exit();
	}
?>