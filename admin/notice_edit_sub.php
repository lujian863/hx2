<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/lib.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$qur = new AllQuery();
	
	extract($_POST);
	extract($_GET);
	unset($_POST,$_GET);
	
	//最后一道验证
	if($n_title == "" || $n_author == "" || $n_time == "" || $n_content == "" || $n_times == ""){
		echo "<script>alert('通知更新失败，请检查信息完整性！');history.back(); </script>";
		exit();
	}
	
	
	if($qur->updateNotice($id,$n_title,$n_author,$n_time,$n_content,$n_times)){
		echo "<script>alert('通知更新成功！');window.close();</script>";
		exit();
	}else{
		echo "<script>alert('通知更新失败，请检数据库连接！');history.back(); </script>";
		exit();
	}
?>