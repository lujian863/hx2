<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加管理员</title>
<link rel="stylesheet" href="css/main.css" />
</head>

<body>
<h2>添加管理员</h2>
<hr />
<form name="f1" method="post" action="admin_add_sub.php" onsubmit="return chk();">
<div id="adminMain">
	<div id="adminTable">
		<div class="adminTableOne">
			<div class="adminTableOneOne">管理员账号：</div>
			<div class="adminTableOneTwo"><input type="text" class="inpBig" name="a_name" id="a_name" /></div>
		</div>
		<div class="adminTableOne">
			<div class="adminTableOneOne">管理员密码：</div>
			<div class="adminTableOneTwo"><input type="password" class="inpBig" name="a_pass_a" id="a_pass_a" /></div>
		</div>
		<div class="adminTableOne">
			<div class="adminTableOneOne">再次输入密码：</div>
			<div class="adminTableOneTwo"><input type="password" class="inpBig" name="a_pass_b" id="a_pass_b"/></div>
		</div>
		<div class="adminTableOne">
			<div class="adminTableOneOne">&nbsp;</div>
			<div class="adminTableOneTwo" align="right"><input type="submit" value="添加管理员" class="button" /></div>
		</div>
	</div>
</div>
</form>
</body>
<script language="javascript">
	function chk(){
		if(document.getElementById("a_name").value == ""){
			alert("请填写账户名！");
			return false;
		}
		if(document.getElementById("a_pass_a").value == "" || document.getElementById("a_pass_b").value == ""){
			alert("请填写密码！");
			return false;
		}
		if(document.getElementById("a_pass_a").value != document.getElementById("a_pass_b").value){
			alert("密码两次输入不一致！");
			return false;
		}
	}
</script>
</html>
