<?php
	/*
	*Project:redcorss.njau.edu.cn-南农红会官网
	*Time:2012-08-10
	*Author:lujian863
	*E-Mail:lujian863@gmail.com
	*/

	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	
	if(!isset($_SESSION['uname'])){
		echo "<script>alert('请登录！');window.location.href='login.html';</script>";
		exit();
	}
?>