<?php
	include("cfm.php");
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$q = new AllQuery();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>通知列表</title>
<link rel="stylesheet" href="css/main.css" />
</head>

<body>
<h2>管理通知</h2>
<hr />
<form name="f1" id="f1" method="post">
	<input type="hidden" name="page_now" value="
	<?php 
		$page_now = 1;
		if(isset($_POST['page_now'])){
			$page_now = $_POST['page_now'];
		}
		if(isset($_GET['page_now'])){
			$page_now = $_GET['page_now'];
		}
		echo $page_now;
	?>" id="page_now" /><!-- 当前是第几页 -->
	<input type="hidden" name="page_num" value="9" id="page_num" /><!-- 一页有几条新闻 -->
</form>
<div class="tab_list">
	<div class="tab_row_x">
		<div class="tab_col_a">系统编号</div>
		<div class="tab_col_c">通知标题</div>
		<div class="tab_col_b">通知来源</div>
		<div class="tab_col_b">发布时间</div>
		<div class="tab_col_a">&nbsp;</div>
	</div>
	<div class="hr_y"></div>
<?php
	
	$page_num = 9;
	$page_now = 1;
	if(isset($_POST['page_now'])){
		$page_now = $_POST['page_now'];
	}
	
	
	//这里获取分页所需的数据
	$total_num = $q->getNoticeNum();//给定通知类别，获取通知数量
	$total_page = ceil($total_num/$page_num);//获取总页数
	
	if($total_num == 0){
		echo "没有通知！";
	}
	
	//打印通知
	$rs_pn = $q->getPageNotice($page_now,$page_num);	
	while($row_pn = mysql_fetch_array($rs_pn)){
?>
	<div class="tab_row">
		<div class="tab_col_a"><?php echo $row_pn['n_id']; ?></div>
		<div class="tab_col_c">
		<a href="#" target="_blank" title="<?php echo $row_pn['n_title']; ?>">
		<?php 
			$tit = $row_pn['n_title'];
			if(mb_strlen($tit,'UTF-8')>20){
				$tit = mb_substr($tit,0,20,'UTF-8');
				$tit .= "...";
			}
			echo $tit; 
		?></a>
		</div>
		<div class="tab_col_b"><?php echo $row_pn['n_author']; ?></div>
		<div class="tab_col_b"><?php echo $row_pn['n_time']; ?></div>
		<div class="tab_col_d_x">
			<input type="button" value="编辑" class="button_x" onclick="editNotice(<?php echo $row_pn['n_id']; ?>);" />
			<input type="button" value="删除" class="button_x" onclick="delNotice(<?php echo $row_pn['n_id']; ?>);" />
		</div>
	</div>
	<div class="hr_x"></div>
<?php
	}
?>

	<div class="hr_y"></div>
	<div class="page_list">
	
<?php
	if($page_now != 1){	
?>
		<input type="button" value="首&nbsp;&nbsp;页" class="button" onclick="startPage();" />
		<input type="button" value="上一页" class="button" onclick="lastPage();" />
<?php 
	}
	//设置快速跳转按钮为5个(5页为一行，最后不足5页的为一行)
	$page_list_num = ceil($total_page/5);//总共有多少行
	$tmp = ceil($page_now/5);//当前是在第几行
	$tmp_mo = 5 - $total_page%5;//不足一行时后面去掉的
	
	$start_page_num = ($tmp-1)*5+1;//起始页码
	$end_page_num = $tmp*5;//结束页码
	if($tmp == $page_list_num && $tmp_mo != 5){//当为最后一行时，减去不足5页的，如果$tmp_mo == 5，则不计
		$end_page_num = $end_page_num - $tmp_mo;
	}
	//for($i = 0; $i < 5; $i++){
	$page_tmp = $start_page_num;
	while($page_tmp <= $end_page_num){
		if($page_tmp != $page_now){
?>
		<input type="button" value="<?php echo $page_tmp; ?>" class="button" onclick="jumpPage(<?php echo $page_tmp; ?>)" />
<?php
		}else{
			echo $page_tmp;
		}
		$page_tmp++;
	}
	if($page_now < $total_page){	
?>
		<input type="button" value="下一页" class="button" onclick="nextPage();" />
		<input type="button" value="末&nbsp;&nbsp;页" class="button" onclick="endPage(<?php echo $total_page; ?>);" />
<?php 
	}
?>	
		共<?php echo $total_page; ?>页
	</div>
</div>
</body>
</html>
<script language="javascript">
function nextPage(){
	/*
	var page_now = document.getElementById("page_now").value + 1;
	var page_num = document.getElementById("page_num").value;
	var n_kind = document.getElementById("n_kind").value;
	*/
	var pn = parseInt(document.getElementById("page_now").value) + 1;
	document.getElementById("page_now").value = pn;
	document.getElementById("f1").submit();
}
function jumpPage(pn){
	document.getElementById("page_now").value = pn;
	document.getElementById("f1").submit();
}
function lastPage(){
	var pn = parseInt(document.getElementById("page_now").value) - 1;
	document.getElementById("page_now").value = pn;
	document.getElementById("f1").submit();
}
function startPage(){
	document.getElementById("page_now").value = 1;
	document.getElementById("f1").submit();
}
function endPage(page_end){
	document.getElementById("page_now").value = page_end;
	document.getElementById("f1").submit();
}



function delNotice(id){
	if(confirm("确认删除此条通知？提示：删除操作不可恢复！")){
		parent.document.getElementById('mainframe').contentWindow.location = 'notice_del.php?id='+id;
	}
}

function editNotice(id){
	window.open('notice_edit.php?id='+id,'_blank','scrollbars=yes,width=1000,height=720');
	//window.showModalDialog("news_edit.php",null,"scroll:yes;center:yes;dialogWidth:1000px;dialogHeight:800px");
}
</script>
