<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户手册</title>
<style type="text/css">
.sys_info{
	font-size:18px;
	font-family:"宋体";
	color:#009900;
}
.sys_info li{
	height:22px;
	margin-top:5px;
}
</style>
</head>

<body>
<h2>用户手册</h2>
<hr />
<div class="sys_info">
<ul>
	<li>1.在编辑新闻时，请不要刷新页面，否则编辑的内容就会全部丢失；</li>
	<li>2.admin管理员具有最高权限，请妥善保管admin管理员的账号密码；</li>
	<li>3.上传文件时，请不要刷新页面，否则会重复上传；</li>
	<li>4.若有疑问，请联系开发人员。</li>
</ul>
</div>
</body>
</html>
