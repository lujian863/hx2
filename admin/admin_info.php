<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理员说明</title>
</head>

<body>
<h2>管理员说明</h2>
<hr />
<div>
<p>
	<span style="font-size:16px;">1. admin 管理员受系统保护，是唯一无法被删除的管理员；</span>
</p>
<p>
	<span style="font-size:16px;">2. 管理员可以进行添加管理员、修改密码、删除管理员(只有 admin 管理员有这个权限)等操作；</span>
</p>
<p>
	<span style="font-size:16px;">3. 若忘记&nbsp;admin 管理员密码，请联系开发者，开发者邮箱见欢迎页面；</span>
</p>
<p>
	<span style="font-size:16px;">4. 建议管理员账号不要超过5个。</span>
</p>
</div>
</body>
</html>
