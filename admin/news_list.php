<?php
	include("cfm.php");
	include("../conf/config.php");
	include("../classes/conn.class.php");
	include("../classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	$q = new AllQuery();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新闻列表</title>
<link rel="stylesheet" href="css/main.css" />
</head>

<body>
<h2>管理新闻</h2>
<hr />
<form name="f_kind" method="post" action="news_list.php" id="kind_form" onsubmit="setPage(1)">
<input type="hidden" name="page_now" value="
	<?php 
		$page_now = 1;
		if(isset($_POST['page_now'])){
			$page_now = $_POST['page_now'];
		}
		if(isset($_GET['page_now'])){
			$page_now = $_GET['page_now'];
		}
		echo $page_now;
	?>" id="page_now" /><!-- 当前是第几页 -->
<input type="hidden" name="page_num" value="8" id="page_num" /><!-- 一页有几条新闻 -->
<div class="select_kind">
	<select name="n_kind" class="sel_list" id="n_kind">
		<?php
			$rs_kind = $q->getKinds();
			while($row_kind = mysql_fetch_array($rs_kind)){
		?>
			<option value="<?php echo $row_kind['k_num']; ?>" 
				<?php 
					$n_kind = 1;
					if(isset($_POST['n_kind'])){
						$n_kind = $_POST['n_kind'];
					}
					if($n_kind == $row_kind['k_num']){
						echo "selected=\"selected\"";
					}
				?>
			><?php echo $row_kind['k_name']; ?></option>
		<?php
			}
		?>
		</select>&nbsp;&nbsp;
		<input type="submit" value="选择新闻类别" class="button" />
</div>
</form>
<br />
<div class="tab_list">
	<div class="tab_row_x">
		<div class="tab_col_a">系统编号</div>
		<div class="tab_col_c">新闻标题</div>
		<div class="tab_col_b">新闻来源</div>
		<div class="tab_col_b">发布时间</div>
		<div class="tab_col_a">新闻类别</div>
		<div class="tab_col_a">&nbsp;</div>
	</div>
	<div class="hr_y"></div>
<?php
	
	$page_num = 8;//每页显示多少条
	if(isset($_POST['page_num'])){
		$page_num = $_POST['page_num'];
	}
	
	//这里获取分页所需的数据
	$total_num = $q->getNewsNum($n_kind);//给定新闻类别，获取新闻数量
	$total_page = ceil($total_num/$page_num);//获取总页数
	
	if($total_num == 0){
		echo "没有新闻！";
	}
	
	//打印新闻
	$rs_pn = $q->getPageNews($page_now,$page_num,$n_kind);	
	while($row_pn = mysql_fetch_array($rs_pn)){
?>
	<div class="tab_row">
		<div class="tab_col_a"><?php echo $row_pn['n_id']; ?></div>
		<div class="tab_col_c">
		<a href="../news_view.php?id=<?php echo $row_pn['n_id']; ?>" target="_blank" title="<?php echo $row_pn['n_title']; ?>">
		<?php 
			$tit = $row_pn['n_title'];
			if(mb_strlen($tit,'UTF-8')>18){
				$tit = mb_substr($tit,0,18,'UTF-8');
				$tit .= "...";
			}
			echo $tit; 
		?></a>
		</div>
		<div class="tab_col_b"><?php echo $row_pn['n_author']; ?></div>
		<div class="tab_col_b"><?php echo $row_pn['n_time']; ?></div>
		<div class="tab_col_a"><?php echo $q->getOneKind($row_pn['n_kind']); ?></div>
		<div class="tab_col_d">
			<input type="button" value="编辑" class="button_x" onclick="editNews('<?php echo $row_pn['n_id']; ?>');" />
			<input type="button" value="删除" class="button_x" onclick="delNews('<?php echo $row_pn['n_id']; ?>');" />
		</div>
	</div>
	<div class="hr_x"></div>
<?php
	}
?>

	<div class="hr_y"></div>
	<div class="page_list">
	
<?php
	if($page_now != 1){	
?>
		<input type="button" value="首&nbsp;&nbsp;页" class="button" onclick="startPage();" />
		<input type="button" value="上一页" class="button" onclick="lastPage();" />
<?php 
	}
	//设置快速跳转按钮为5个(5页为一行，最后不足5页的为一行)
	$page_list_num = ceil($total_page/5);//总共有多少行
	$tmp = ceil($page_now/5);//当前是在第几行
	$tmp_mo = 5 - $total_page%5;//不足一行时后面去掉的
	
	$start_page_num = ($tmp-1)*5+1;//起始页码
	$end_page_num = $tmp*5;//结束页码
	if($tmp == $page_list_num && $tmp_mo != 5){//当为最后一行时，减去不足5页的，如果$tmp_mo == 5，则不计
		$end_page_num = $end_page_num - $tmp_mo;
	}
	//for($i = 0; $i < 5; $i++){
	$page_tmp = $start_page_num;
	while($page_tmp <= $end_page_num){
		if($page_tmp != $page_now){
?>
		<input type="button" value="<?php echo $page_tmp; ?>" class="button" onclick="jumpPage(<?php echo $page_tmp; ?>)" />
<?php
		}else{
			echo $page_tmp;
		}
		$page_tmp++;
	}
	if($page_now < $total_page){	
?>
		<input type="button" value="下一页" class="button" onclick="nextPage();" />
		<input type="button" value="末&nbsp;&nbsp;页" class="button" onclick="endPage(<?php echo $total_page; ?>);" />
<?php 
	}
?>	
		共<?php echo $total_page; ?>页
	</div>
</div>

</body>
</html>
<script language="javascript">
function nextPage(){
	/*
	var page_now = document.getElementById("page_now").value + 1;
	var page_num = document.getElementById("page_num").value;
	var n_kind = document.getElementById("n_kind").value;
	*/
	var pn = parseInt(document.getElementById("page_now").value) + 1;
	document.getElementById("page_now").value = pn;
	document.getElementById("kind_form").submit();
}
function jumpPage(pn){
	document.getElementById("page_now").value = pn;
	document.getElementById("kind_form").submit();
}
function lastPage(){
	var pn = parseInt(document.getElementById("page_now").value) - 1;
	document.getElementById("page_now").value = pn;
	document.getElementById("kind_form").submit();
}
function startPage(){
	document.getElementById("page_now").value = 1;
	document.getElementById("kind_form").submit();
}
function endPage(page_end){
	document.getElementById("page_now").value = page_end;
	document.getElementById("kind_form").submit();
}
function setPage(pn){
	document.getElementById("page_now").value = pn;
}


function delNews(id){
	if(confirm("确认删除此条新闻？提示：删除操作不可恢复！")){
		parent.document.getElementById('mainframe').contentWindow.location = 'news_del.php?id='+id;
	}
}

function editNews(id){
	window.open('news_edit.php?id='+id,'_blank','scrollbars=yes,width=1000,height=700,top=0,left=0');
}
</script>
