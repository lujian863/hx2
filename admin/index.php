﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
ProName:redcorss.njau.edu.cn
Time:2012-08-10
Author:lujian863
E-Mail:lujian863@gmail.com
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>后台管理系统</title>
<link rel="Shortcut Icon" href="resources/images/ico/logo.ico" />
<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />
<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
<script type="text/javascript" src="resources/scripts/facebox.js"></script>
<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>
</head>
<?php
	include("cfm.php");
?>
<body onload="init();">

<div id="body-wrapper">

  <!-- 左边导航栏开始 -->
  <div id="sidebar">
    <div id="sidebar-wrapper">
      <h1 id="sidebar-title"><a href="#"></a></h1>
      <img id="logo" src="resources/images/logo.png" alt="技术支持：" />
      <div id="profile-links">
      </div>
      <ul id="main-nav">
        <li> <a href="#" class="nav-top-item no-submenu" onclick="addDao('welcome.php','后台首页-》欢迎','500px');">欢迎</a></li>
        
		 <li> <a href="#" class="nav-top-item">新闻通知管理</a>
          <ul>
		  	<li><a href="#" onclick="addDao('news_list.php','新闻通知管理——》管理新闻','500px');">管理新闻</a></li>
            <li><a href="#" onclick="addDao('news_add.php','新闻通知管理——》添加新闻','820px');">添加新闻</a></li>
			<li><a href="#" onclick="addDao('notice_list.php','新闻通知管理——》管理通知','500px');">管理通知</a></li>
            <li><a href="#" onclick="addDao('notice_add.php','新闻通知管理——》添加通知','820px');">添加通知</a></li>
          </ul>
        </li>
		
		 <li> <a href="#" class="nav-top-item">下载中心管理</a>
          <ul>
			<li><a href="#" onclick="addDao('file_upload.php','下载中心管理——》文件上传','500px');">文件上传</a></li>
            <li><a href="#" onclick="addDao('file_list.php','下载中心管理——》文件管理','500px');">文件管理</a></li>
			<!--
			<li><a href="#" onclick="addDao('file_kind.php','下载中心管理——》管理文件分类','500px');">管理文件分类</a></li>
          	-->
		  </ul>
        </li>
				
		
		<li> <a href="#" class="nav-top-item">其他内容管理</a>
          <ul>
		  	<li><a href="#" onclick="addDao('vip.php','其他内容管理-》会员中心','400px');">会员中心</a></li>
			<li><a href="#" onclick="addDao('about_us.php','其他内容管理-》关于我们','700px');">关于我们</a></li>
			<li><a href="#" onclick="addDao('links_manage.php','其他内容管理——》友情链接','500px');">友情链接</a></li>
		  </ul>
        </li>
		
		<li> <a href="#" class="nav-top-item">管理员</a>
          <ul>
		  	<li><a href="#" onclick="addDao('admin_info.php','管理员-》管理员说明','500px');">管理员说明</a></li>
			<li><a href="#" onclick="addDao('admin_list.php','管理员-》管理员列表','500px');">管理员列表</a></li>
			<li><a href="#" onclick="addDao('admin_add.php','管理员-》添加管理员','500px');">添加管理员</a></li>
          </ul>
        </li>
		
		<!--
		<li> <a href="#" class="nav-top-item">回收站</a>
          <ul>
			<li><a href="#">回收站说明</a></li>
			<li><a href="#">新闻回收站</a></li>
            <li><a href="#">通知回收站</a></li>
          </ul>
        </li>
		-->
			
		 <li> <a href="#" class="nav-top-item">系&nbsp;&nbsp;统</a>
          <ul>
            <li><a href="../index.php" target="_blank">查看首页</a></li>
			<li><a href="#" onclick="addDao('user_book.php','系统-》用户手册','500px');">用户手册</a></li>
			<li><a href="#" onclick="addDao('sys_info.php','系统-》系统信息','500px');">系统信息</a></li>
            <li><a href="logout.php" onclick="return cfm();">安全退出</a></li>
          </ul>
        </li>
		
        <li></li>
        <li></li>
      </ul>
    </div>
  </div>
  <!-- 左边栏结束 -->
   <div id="main-content">
    <noscript>
    </noscript>
<div class="clear"></div>
    <div class="content-box">
<div class="content-box-content">
        <div class="tab-content default-tab" id="tab1">
          <div class="notification attention png_bg"> <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="关闭位置提示窗口" alt="close" /></a>
            <div id="maininfo">现在位置：
			</div>
          </div>

<!--正文开始-->
 <iframe style="width:100%; overflow:auto;"; id="mainframe">
 </iframe>
<!--正文结束-->		 
        </div>
        <div class="tab-content" id="tab2"></div>
      </div>
    </div>
<div class="clear"></div>
  </div>
</div>
</body>
</html>
<script language="javascript">
function init(){
	addDao('welcome.php','后台首页');
	document.getElementById("mainframe").style.height="500px";
}
function addDao(url,info,hi){
	if(hi == '0'){
		hi = '720';
	}
	document.getElementById("mainframe").style.height= hi;
	document.getElementById("mainframe").src = url;
	document.getElementById("maininfo").innerHTML = "现在位置：" + info;
}
function cfm(){
	return confirm("确认退出？")
}
</script>