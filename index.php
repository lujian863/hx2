<?php

/*
*Project:redcorss.njau.edu.cn-南农红会官网
*Time:2012-08-10
*Author:lujian863
*E-Mail:lujian863@gmail.com
*/


	session_start();
	header('Content-Type:text/html;Charset=utf-8;');
	include("inc/top.php");
	include("conf/config.php");
	include("classes/conn.class.php");
	include("classes/lib.class.php");
	include("classes/query.class.php");
	
	$dbc = new DBConn();
	$dbc->connect_server();
	$dbc->connect_db();
	
	//主页每浏览一次，网站浏览量增加一次
	$lib = new Libs();
	$lib->addReadTimes();
	
	$q = new AllQuery();
	
	extract($_POST);
	extract($_GET);
	unset($_POST,$_GET);
?>
<div id="section_final">

		<div id="section1_left">
			
			
			<div id="gonggao" style=" height:368px;">
    		<div id="r_title_wrap">
        		<div id="r_title"><span style="float:left;">最新通知</span><span id="t"><a href="notice_list.php">更多>></a></span></div>
        	</div>
        	<div id="gonggao_c" style="height:320px;">
        	<ul>
<?php
	$rs_notice = $q->getNoticeList(12);
	while($row_notice = mysql_fetch_array($rs_notice)){
?>
        		<li><a href="notice_view.php?id=<?php echo $row_notice['n_id']; ?>" title="<?php echo $row_notice['n_title']; ?>">
				<?php 
					$tit = $row_notice['n_title'];
					if(mb_strlen($tit,'UTF-8')>16){
						$tit = mb_substr($tit,0,16,'UTF-8');
						$tit .= "...";
					}
					echo $tit; 
				?>
				</a></li>
<?php
	}
?>
       		</ul>
			</div>
    	</div>
			
		<div id="gonggao" style="height:180px;margin-top:5px;">
		<div id="r_title_wrap"><div id="r_title">会员登录</div></div>
		<div id="gonggao_c" style="height:140px;">
			<div id="login_l">
			<ul>
				<li>用户名：<span><input type="text" id="login_input" /></span></li>
				<li>密<span style="margin-left:1em;">码</span>：<span><input type="password" id="login_input" /></span></li>
				<li style="height:32px !important ;*height:30px;"><img src="images/login.png"  style="margin-left:40px;"/>
				<span style="line-height:22px;margin-top:8px; font-size:12px;">忘记密码？</span></li>
			</ul>
			</div>
		</div>
			
			
		</div>

</div>

<div id="section1_right">  
   	<div id="dongtai">
    	<div id="d_title_wrap">
        <div id="d_title">
        	<span style="float:left;">最新动态</span>
    		<a href="#" target="_blank"><span id="t">更多>></span></a>
        </div></div>
    	<div id="dongtai_c">
		<div id="picBox">
			<ul id="show_pic" style="left:0;">
<?php
	$js = 0;
	$n_rs = $q->getNewsPicList(5);
	while($n_row = mysql_fetch_array($n_rs)){
?>
    			<li><a href="news_view.php?id=<?php echo $n_row['n_id']; ?>" target="_blank" ><img src="admin/kindeditor/attached/image/<?php echo substr($n_row['n_pic'],0,8); ?>/<?php echo $n_row['n_pic']; ?>" width="360" height="260" alt="<?php echo $n_row['n_title'];?>" title="<?php echo $n_row['n_title'];?>" /></a></li>
<?php
		$js++;
	}
?>
			</ul>
    		<ul id="icon_num">
<?php
	for($i = 1; $i <= $js; $i ++){
?>
    			<li 
					<?php 
						if($i == 0) { 
							echo "class=\"active\""; 
						} 
					?> 
				>
				<?php echo $i; ?></li>
 <?php
 		
 	}
 ?>
			</ul>		
		</div>
		
        <div id="liebiao">
        	<div id="s1_c">
    		<ul>
<?php
	$rs_news = $q->getNewsList(9,1);
	while($row_news = mysql_fetch_array($rs_news)){
?>
				<!--li有字数限制 22个汉字字符-->
				<li><a href="news_view.php?id=<?php echo $row_news['n_id']; ?>" title="<?php echo $row_news['n_title']; ?>" target="_blank">
				<?php 
					$tit = $row_news['n_title'];
					if(mb_strlen($tit,'UTF-8')>22){
						$tit = mb_substr($tit,0,22,'UTF-8');
						$tit .= "...";
					}
					echo $tit; 
				?>
				</a></li>
<?php
	}
?>		
    		</ul>
    		</div>
        </div>
        </div>
    </div>
  
    <div id="zhibu">
    	<div id="s2_title_wrap">
    	<div id="s2_title"><span style="float:left;">特色活动</span>
    		<a href="#" target="_blank"><span id="t">更多>></span></a>
    	</div>
        </div>
    	<div id="s2_c">
    	<ul>
<?php
	$rs_news = $q->getNewsList(9,2);
	while($row_news = mysql_fetch_array($rs_news)){
?>
				<!--li有字数限制 17个汉字字符-->
				<li>
					<span id="s1_c_sub">【特色活动】</span>
					<a href="news_view.php?id=<?php echo $row_news['n_id']; ?>" title="<?php echo $row_news['n_title']; ?>" target="_blank">
					<?php 
					$tit = $row_news['n_title'];
					if(mb_strlen($tit,'UTF-8')>18){
						$tit = mb_substr($tit,0,18,'UTF-8');
						$tit .= "...";
					}
					echo $tit; 
					?></a>
				</li>
<?php
	}
?>
    	</ul>
    	</div>
    </div>
    
    <div id="quntuan">
    	<div id="s2_title_wrap1">
    	<div id="s2_title"><span style="float:left;">志愿者服务</span>
    	<a href="#" target="_blank"><span id="t">更多>></span></a>
    	</div></div>
    	<div id="s2_c">
    	<ul>
<?php
	$rs_news = $q->getNewsList(9,3);
	while($row_news = mysql_fetch_array($rs_news)){
?>
				<!--li有字数限制 17个汉字字符-->
				<li>
					<span id="s1_c_sub">【志愿者服务】</span>
					<a href="news_view.php?id=<?php echo $row_news['n_id']; ?>" title="<?php echo $row_news['n_title']; ?>" target="_blank">
					<?php 
					$tit = $row_news['n_title'];
					if(mb_strlen($tit,'UTF-8')>18){
						$tit = mb_substr($tit,0,18,'UTF-8');
						$tit .= "...";
					}
					echo $tit; 
					?>
					</a>
				</li>
<?php
	}
?>
    	</ul>
    	</div>
    </div>
    
</div>
</div>

<div id="section_link">
  <div id="lian"><div id="lian_head">友情链接：</div></div>
    <div class="lian_t">
    <ul>
	<?php
		$rs = $lib->getLinks();
		while($row = mysql_fetch_array($rs)){
	?>
    	<li><a href="<?php echo $row['l_url'];?>" target="_blank"><?php echo $row['l_name'];?></a></li>
    <?php
		}
	?>
    </ul>
    
    </div>
</div>


<?php
	include("inc/bottom.php");
?>
<div style="display:none">
<script type="text/javascript">
/**
 *glide.layerGlide((oEventCont,oSlider,sSingleSize,sec,fSpeed,point);
 *@param auto type:bolean 是否自动滑动 当值是true的时候 为自动滑动
 *@param oEventCont type:object 包含事件点击对象的容器
 *@param oSlider type:object 滑动对象
 *@param sSingleSize type:number 滑动对象里单个元素的尺寸（width或者height）  尺寸是有point 决定
 *@param second type:number 自动滑动的延迟时间  单位/秒
 *@param fSpeed type:float   速率 取值在0.05--1之间 当取值是1时  没有滑动效果
 *@param point type:string   left or top
 */
var glide =new function(){
	function $id(id){return document.getElementById(id);};
	this.layerGlide=function(auto,oEventCont,oSlider,sSingleSize,second,fSpeed,point){
		var oSubLi = $id(oEventCont).getElementsByTagName('li');
		var interval,timeout,oslideRange;
		var time=1; 
		var speed = fSpeed 
		var sum = oSubLi.length;
		var a=0;
		var delay=second * 1000; 
		var setValLeft=function(s){
			return function(){
				oslideRange = Math.abs(parseInt($id(oSlider).style[point]));	
				$id(oSlider).style[point] =-Math.floor(oslideRange+(parseInt(s*sSingleSize) - oslideRange)*speed) +'px';		
				if(oslideRange==[(sSingleSize * s)]){
					clearInterval(interval);
					a=s;
				}
			}
		};
		var setValRight=function(s){
			return function(){	 	
				oslideRange = Math.abs(parseInt($id(oSlider).style[point]));							
				$id(oSlider).style[point] =-Math.ceil(oslideRange+(parseInt(s*sSingleSize) - oslideRange)*speed) +'px';
				if(oslideRange==[(sSingleSize * s)]){
					clearInterval(interval);
					a=s;
				}
			}
		}
		
		function autoGlide(){
			for(var c=0;c<sum;c++){oSubLi[c].className='';};
			clearTimeout(interval);
			if(a==(parseInt(sum)-1)){
				for(var c=0;c<sum;c++){oSubLi[c].className='';};
				a=0;
				oSubLi[a].className="active";
				interval = setInterval(setValLeft(a),time);
				timeout = setTimeout(autoGlide,delay);
			}else{
				a++;
				oSubLi[a].className="active";
				interval = setInterval(setValRight(a),time);	
				timeout = setTimeout(autoGlide,delay);
			}
		}
	
		if(auto){timeout = setTimeout(autoGlide,delay);};
		for(var i=0;i<sum;i++){	
			oSubLi[i].onmouseover = (function(i){
				return function(){
					for(var c=0;c<sum;c++){oSubLi[c].className='';};
					clearTimeout(timeout);
					clearInterval(interval);
					oSubLi[i].className="active";
					if(Math.abs(parseInt($id(oSlider).style[point]))>[(sSingleSize * i)]){
						interval = setInterval(setValLeft(i),time);
						this.onmouseout=function(){if(auto){timeout = setTimeout(autoGlide,delay);};};
					}else if(Math.abs(parseInt($id(oSlider).style[point]))<[(sSingleSize * i)]){
							interval = setInterval(setValRight(i),time);
						this.onmouseout=function(){if(auto){timeout = setTimeout(autoGlide,delay);};};
					}
				}
			})(i)			
		}
	}
}
glide.layerGlide(true,'icon_num','show_pic',360,3,0.1,'left');
</script>
</div>
<!-- TianJi Button BEGIN -->
<script type="text/javascript"> 
	var tianji_config = {
		url: "http://redcross.njau.edu.cn/",
		title: "南京农业大学红十字会",
		siteName:"南京农业大学红十字会"
	}
</script>
<script type="text/javascript" src="http://tj.see-say.com/code/tianji_r.js?move=0" charset="utf-8"></script>
<!-- TianJi Button END -->
